var assert = require('assert');

var endTest = true;

describe('open browser and check state', function () {

    it('should resize the current window to 1200 x 768 px', function () {
        browser.setViewportSize({
            width: 1200,
            height: 768
        });
    });

    it('open UCRM app and checks state - login or proceed wizard', function () {
        browser.url('http://web_app/');
        browser.pause('1000');
        var url = browser.getUrl();
        if (url == 'http://web_app/wizard/account-setup') {
            // new setup with saved session
            var title = browser.getTitle();
            assert.equal(title, 'Setup wizard - UCRM');
        } else if (url == 'http://web_app/login') {
            //login screen
            browser.pause('1000');
            browser.setValue('input[name="_username"]', 'admin');
            browser.setValue('input[name="_password"]', 'Drny92D');
            browser.click('button[name="_submit"]');
            browser.pause('2000');
        }
    });

    it('recheck UCRM app state - continue wizard or quits', function () {
        browser.url('http://web_app/');
        browser.pause('1000');
        var url = browser.getUrl();
        if (url === 'http://web_app/wizard/account-setup') {
            // new setup with saved session
            var title = browser.getTitle();
            assert.equal(title, 'Setup wizard - UCRM');
            endTest = false;
            browser.pause('1000');
            browser.setValue('input#wizard_password', 'Drny92D');
            browser.setValue('input#wizard_email', 'admin@example.com');
        } else if (url === 'http://web_app/') {
            //saved and logged session
            var title = browser.getTitle();
            assert.equal(title, 'UCRM');
            endTest = true;

        }
    });


    describe('continue test?', function () {

        it('quits test or not', function () {

            if (endTest) {
                //nothing to do
                assert.equal('UCRM', 'webapp already setted up or in uknown state');

            } else {

                describe('continue tests', function () {

                    it('change timezone and send form', function () {
                        var timezone = browser.getAttribute('span#select2-wizard_timezone-container', 'title');
                        console.log('------------------ ' + timezone);
                        // clicks on dropdown
                        browser.click('span.select2-selection--single');
                        // depends on autocompletion, sends few letters and than return
                        browser.setValue('input.select2-search__field', 'prag\uE006');
                        var timezone = browser.getAttribute('span#select2-wizard_timezone-container', 'title');
                        console.log('------------------ ' + timezone);
                        assert.equal(timezone, '(UTC+01:00) Belgrade, Bratislava, Budapest, Ljubljana, Prague');
                        browser.click('button[type="submit"]');
                    });

                    it('loads 2nd step in wizard', function () {
                        browser.pause('500');
                        var url = browser.getUrl();
                        // url is name of the web_app container 
                        assert.equal(url, 'http://web_app/wizard/organization');
                    });

                    it('fill username and password', function () {
                        browser.pause('500');
                        browser.setValue('input#organization_wizard_name', 'Organization Name');
                        browser.setValue('input#organization_wizard_street1', 'StreetName 1');
                        browser.setValue('input#organization_wizard_city', 'CityName 2');
                        browser.setValue('input#organization_wizard_zipCode', '12345');
                        browser.setValue('input#organization_wizard_email', 'admin@example.com');
                    });

                    it('change country in step 2', function () {
                        var country = browser.getAttribute('span#select2-organization_wizard_country-container', 'title');
                        // clicks on dropdown
                        browser.click('span.select2-selection--single');
                        // depends on autocompletion, sends few letters and than return
                        browser.setValue('input.select2-search__field', 'cze\uE006');
                    });

                    it('verify step 2 and submit form', function () {
                        browser.pause('500');
                        var country = browser.getAttribute('span#select2-organization_wizard_country-container', 'title');
                        console.log('------------------ ' + country);
                        assert.equal(country, 'Czech Republic');
                        assert.equal(browser.getValue('input#organization_wizard_name'), 'Organization Name');
                        assert.equal(browser.getValue('input#organization_wizard_street1'), 'StreetName 1');
                        assert.equal(browser.getValue('input#organization_wizard_city'), 'CityName 2');
                        assert.equal(browser.getValue('input#organization_wizard_zipCode'), '12345');
                        assert.equal(browser.getValue('input#organization_wizard_email'), 'admin@example.com');
                        browser.click('button[type="submit"]');
                    });

                    it('loads 3rd step in wizard', function () {
                        browser.pause('500');
                        var url = browser.getUrl();
                        // url is name of the web_app container 
                        assert.equal(url, 'http://web_app/wizard/mailer');

                        // input setting_mailer_mailerSenderAddress
                        // a href="/wizard/demo-mode"  SKIP
                    });

                    it('clikcs skip button (skips mailer conf)', function () {
                        browser.pause('500');
                        //var skip_btn = selectBox.selectByAttribute('href', '/wizard/demo-mode"');
                        browser.click('a.button--transparent');
                    });

                    it('loads 4th step in wizard', function () {
                        browser.pause('500');
                        var url = browser.getUrl();
                        // url is name of the web_app container 
                        assert.equal(url, 'http://web_app/wizard/demo-mode');

                        // input setting_mailer_mailerSenderAddress
                        // a href="/wizard/demo-mode"  SKIP
                    });

                    it('clikcs "Continue in demo mode" button', function () {
                        browser.pause('500');
                        //var skip_btn = selectBox.selectByAttribute('href', '/wizard/demo-mode"');
                        browser.click('a.button--warning-fill');
                    });
                });
            } //quit test or not
        });

    });
});